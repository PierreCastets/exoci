from flask import Flask
import module as md

app = Flask(__name__)

@app.route("/square/<num>")
def display_square(num):
    num = int(num)
    return (f"Le carré de {num} est {md.square(num)}")

app.run(host="0.0.0.0", port=8080)