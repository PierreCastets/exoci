import unittest
import module as md

class TestSquare(unittest.TestCase):
    def test_square(self):
        self.assertEqual(md.square(0), 0)
        self.assertEqual(md.square(1), 1)
        self.assertEqual(md.square(-3), 9)